package com.ichmal.helloworld.model;

import java.util.ArrayList;
import java.util.List;

public class GanjilGenap {
    private int bil1, bil2;

    public int getBil1() {
        return bil1;
    }

    public void setBil1(int bil1) {
        this.bil1 = bil1;
    }

    public int getBil2() {
        return bil2;
    }

    public void setBil2(int bil2) {
        this.bil2 = bil2;
    }

    public List<String> getHasil(){
        List<String> hasil = new ArrayList<>();
        for(int i = bil1; i <= bil2; i++){
            if(i % 2 == 0){
                hasil.add(i+" Adalah Genap");
            } else{
                hasil.add(i+ "Adalah Ganjil");
            }
        }
        return hasil;
    }
}
