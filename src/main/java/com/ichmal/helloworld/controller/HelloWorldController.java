package com.ichmal.helloworld.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class HelloWorldController {
    @GetMapping("/helloworld")
    public String index(){
        return "Hello World";
    }
}
