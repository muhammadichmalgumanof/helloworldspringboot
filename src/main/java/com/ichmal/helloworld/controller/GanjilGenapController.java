package com.ichmal.helloworld.controller;

import com.ichmal.helloworld.model.GanjilGenap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class GanjilGenapController {
    @GetMapping("/ganjilGenap")
    public String ganjilGenapForm(Model model){
        model.addAttribute("ganjilgenap", new GanjilGenap());
        return "index";
    }
}
